function theta = GradientDescentMethod(X, theta, y, a, n)
%GradientDescentMethod 此处显示有关此函数的摘要
%   梯度下降函数
    % 分配临时theta内存
    loopTheta = zeros(n + 1, 1);
    % 统计维度
    dimension = size(X, 2);
    % 循环样本
    for i = 1:n
        sumX = (X*theta - y) * X(i, 1);
    end
    % 循环维度
    for i = 1:dimension
        % 计算差
        loopTheta(i, 1) = theta(i, 1) - a * (1/dimension) * sumX;  % 处理导数部分
    end
    theta = loopTheta;
end

