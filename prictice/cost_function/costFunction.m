function J = costFunction(X, y, theta)
    m = size(X, 1);                 % 总样本数
    hx = X * theta;                 % 所有样本的回归方程
    sqrErrors = (hx - y) .^2;       % 差的平方的部分
    J = 1/(2*m) * sum(sqrErrors);   % 损失函数
end