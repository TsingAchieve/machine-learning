# 机器学习

## 监督学习

### 概念

> 监督学习是从标记的训练数据来推断一个功能的机器学习任务

- 例如房价的分析

### 工作流程

1、提供数据集

2、提供学习算法

3、利用学习算法输出一个函数

## 无监督学习

### 聚类算法

## 代价函数-拟合（回归的过程就是拟合）

### 代价函数-损失函数概念

site：https://www.zhihu.com/question/310757247/answer/585807112
$$
假设函数：h_\theta(x)=\theta_0+\theta_1x
$$

note：不同的参数可以得到不同的h函数，也就是不同的拟合函数
$$
\theta_0和\theta_1为选择的不同参数
$$

| theta_0 | theta_1 |
| ------- | ------- |
| 1.5     | 0       |
| 0       | 0.5     |
| 1       | 0.5     |

1、如何将给出的数据尽可能的拟合出我们想要的函数呢？

2、选择theta_0和theta_1以便于h函数接近于我们的y（在输入x时）

3、使这个差值平方的求和尽可能小，除以M是为了得到每个的样本，除以2是为了方便后续求导
$$
h(x^i)为拟合函数输出值\\
y^i为数据集中的值\\
代价函数-损失函数：J(\theta_0,\theta_1) = \frac{1}{2M}\sum_{i=1}^{M}(h(x^i)-y^i)^2
$$
注意：

- **公式这么写实际上是为了消除负号的影响，因为求导之后才是我们实际想要的最小误差均值**
- **说白了损失函数就是求出使预测值和实际值差距最小的时候的参数**

### 拟合函数求损失函数

> 根据不同的θ取值，画出损失函数的图像

![拟合函数与损失函数之间的联系](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E6%8B%9F%E5%90%88%E5%87%BD%E6%95%B0%E4%B8%8E%E6%8D%9F%E5%A4%B1%E5%87%BD%E6%95%B0%E4%B9%8B%E9%97%B4%E7%9A%84%E8%81%94%E7%B3%BB.jpg)

Note： **损失函数的极小值就能很好的拟合数据集上的所有数据**

## 梯度下降

> 用此方法得到最小化的J值

![gradient_descent](https://gitee.com/TsingAchieve/notebook-image/raw/master/gradient_descent.png)

- 根据不同的θ值会得到不同的最优解

### 步骤

$$
\begin{align}
&假设损失函数：J(\theta_0,\theta_1)\\
&使损失函数：\min_{\theta_0,\theta_1}J(\theta_0,\theta_1)\\
&定义一些\theta_0,\theta_1\\
&改变\theta_0,\theta_1的值为了减少J函数的值，直到达到J的最小值
\end{align}
$$



### 算法（固定）

$$
\theta_j:=\theta_j - \alpha\frac{\partial J(\theta_0,\theta_1)}{\partial\theta_j}	(j = 0 and j = 1)
$$

Note：

- **α表示梯度下降的快慢即学习效率**
- 

### 总结

- **α表示梯度下降的快慢即学习效率，控制θ更新的快慢**
- **需要同时更新θ0与θ1**
- **:=表示赋值**

![gradient_descent_total](https://gitee.com/TsingAchieve/notebook-image/raw/master/gradient_descent_total.jpg)

## Ⅳ-梯度下降

$$
\frac{\partial}{\partial\theta_j}J(\theta_0,\theta_1) = \frac{\partial}{\partial\theta_j}\frac{1}{2M}\sum_{i=1}^{M}(h(x^i)-y^i)^2 = 
\frac{\partial}{\partial\theta_j}\frac{1}{2M}\sum_{i=1}^{M}(\theta_0 + \theta_1x^i - y^i)^2
$$

## Ⅳ-Ⅰ多元梯度下降

$$
\begin{align*}
&假设：h_\theta(x) = \theta_Tx = \theta_0x_0 + \theta_1x_1 + ... + \theta_nx_n\\
&参数：\theta_0,\theta_1,\theta_2,\theta_3 ... , \theta_n\\
&损失函数：J(\theta_0,\theta_1,\theta_2,\theta_3 ... , \theta_n) = \frac{1}{2M}\sum_{i=1}^{M}(h_\theta(x^i)-y^i)^2\\
&梯度函数：\theta_j := \theta_j - \alpha\frac{\partial}{\partial\theta_j}J(\theta_0,\theta_1,\theta_2,\theta_3 ... , \theta_n)
\end{align*}
$$

![多元梯度](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E5%A4%9A%E5%85%83%E6%A2%AF%E5%BA%A6.jpg)

### Ⅳ-Ⅰ-Ⅰ特征尺度-归一化处理

> 使每个变量乘除某个数让其均保持在特定的范围内

![归一化处理](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E5%BD%92%E4%B8%80%E5%8C%96%E5%A4%84%E7%90%86.jpg)

### Ⅳ-Ⅰ-Ⅱ学习率

> 确保梯度下降函数正确工作

![迭代次数与差值的关系](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E8%BF%AD%E4%BB%A3%E6%AC%A1%E6%95%B0%E4%B8%8E%E5%B7%AE%E5%80%BC%E7%9A%84%E5%85%B3%E7%B3%BB.jpg)

- 梯度持续下降那么J的值则会越来越小，如果是越来越小，那么表示函数正确工作

- 一般地如果出现发散则为学习效率过大，因此应该选择较小的学习效率

- 一般地通过十倍关系去确定α的值，如0.00001,0.0001,0.001,0.01,0.1,1,10,...

## Ⅳ-Ⅱ特征多项式回归

`example`

> 预测房子价格

$$
假设的拟合函数：h_\theta(x) = \theta_0 + \theta_1 \times frontage + \theta_2 \times depth
$$

> 因为房子的长宽都知道，那么我们假设一个新的变量为面积

$$
\begin{align*}
&假设面积的拟合函数：h_\theta(x) = \theta_0 + \theta_1 x\\
&x = frontage \times depth为房子的面积
\end{align*}
$$

- 有时候重新定义一个新的特征可能会得到更好的模型
- 用函数去拟合数据集的时候可以选择不同的函数去拟合

## Ⅳ-Ⅲ正规方程-最小二乘法

$$
\begin{align*}
&ituition:If\space1D\space(\theta\in R)\\
&J(\theta) = a\theta^2 + b\theta + c
\end{align*}
$$

> 利用偏导数一次对各个θ进行求导，然后使其为零，解出θ的向量值；但是我们不推荐这样做

### Ⅳ-Ⅱ-Ⅰ最小二乘法实例

| 固定常数x_0 | 面积(x_1) | 房间数量(x_2) | 层数(x_3) | 使用时长(x_4) | 价格(y) |
| ----------- | --------- | ------------- | --------- | ------------- | ------- |
| 1           | 2104      | 5             | 1         | 45            | 460     |
| 1           | 1416      | 3             | 2         | 40            | 232     |
| 1           | 1534      | 3             | 2         | 30            | 315     |
| 1           | 852       | 2             | 1         | 36            | 178     |

$$
\begin{align*}
X = \left[
\matrix{
  1 & 2104 & 5 & 1 & 45\\
  1 & 1416 & 3 & 2 & 40\\
  1 & 1534 & 3 & 2 & 30\\
  1 & 852 & 2 & 1 & 36
}
\right]
y = \left[\matrix{460\\232\\315\\178}\right]
\end{align*}
$$

> 结合上述矩阵利用下面的公式求出θ

$$
最小二乘法矩阵形式：\theta = (X^TX)^{-1}X^Ty
$$

$$
examples\space(x^{(1)},y^{(1)}),(x^{(2)},y^{(2)}),\cdots,(x^{(m)},y^{(m)});n个特征\\
x^{(i)} = \left[
\matrix{x_0^{(i)}\\x_1^{(i)}\\x_2^{(i)}\\\cdots\\x_n^{(i)}}
\right]\in R^{n+1}
$$

$$
E.g\space\space\space if x^(i) = \left[\matrix{1\\x_1^{(i)}}\right]
$$

### Ⅳ-Ⅱ-Ⅱ正规方程不可逆

- 找出近似的相关的x变量，剔除维度

## Ⅳ-Ⅳ总结

> 选择梯度下降

- 需要选择学习效率α
- 样本维度n比较大
- 需要迭代

> 选择正规方程

- 不需选择学习效率α
- 不需要迭代
- 样本维度n较小
- 需要计算机计算逆矩阵

## Ⅵ-离散逻辑回归

### Ⅵ-Ⅰ分类

- 对于离散的数据集我们就不能用线性回归去拟合，需要使用逻辑回归

### Ⅵ-Ⅱ假设陈述

$$
\begin{align*}
& 逻辑回归模型\\
& 使\space\space0 \leq h_\theta(x) \leq 1\\
& h_\theta(x) = g(\theta^Tx),g(z) = \frac{1}{1+e^{-z}}\\
& h_\theta(x) = \frac{1}{1+e^{-\theta^Tx}}
\end{align*}
$$

![logistic_function](https://gitee.com/TsingAchieve/notebook-image/raw/master/logistic_function.jpg)
$$
\begin{align*}
& Example: If\space\space x = \left[\matrix{x_0\\x_1}\right] = \left[\matrix{2\\tumorSize}\right]\\
& h_\theta(x) = 0.7\\
& P(y = 0|x;\theta) + P(y = 1|x;\theta) = 1\\
& P(y = 0|x;\theta) =1 - P(y = 1|x;\theta)
\end{align*}
$$

### Ⅵ-Ⅲ决策界限

> 以固定函数为依据，根据不同的参数进行判断取值

![decision](https://gitee.com/TsingAchieve/notebook-image/raw/master/decision.jpg)

- 1、使y=1时，z的取值大于零，那么条件成立
- 2、使y=0时，z的取值小于零，那么条件成立

> example-one

![decision-example](https://gitee.com/TsingAchieve/notebook-image/raw/master/decision-example.jpg)
$$
\begin{align*}
& 假设Z的函数为h_\theta(x) = g(\theta_0 + \theta_1x_1 + \theta_2x_2)\\
& 且假设\theta的参数确定,\theta = \left[\matrix{-3 & 1 & 1}\right]^T\\
& 预测使y = 1,则让Z\geq0,故使-3 + x_1+ x_2\geq0,\Rightarrow x_1 + x_2\geq3\\
& 边界函数：x_1+ x_2= 3
\end{align*}
$$
说明：

- g(z)是一个固定的逻辑分类函数，它有左侧和右侧，左侧等于零，右侧等于一，这是固定的
- 为了使上面的g(z)成立，那么就有z的取值范围的关系，这样就把数据集进行了分类
- z的函数就是边界决策函数

> example-two

![decision-example-two](https://gitee.com/TsingAchieve/notebook-image/raw/master/decision-example-two.jpg)

- **对于不同维度的特征向量，我们可以在原有基础上添加更高阶多项式**

$$
\begin{align*}
& 对于正负样本，近似可以用圆去决策分界，故取\theta = \left[\matrix{-1 & 0 & 0 & 1 & 1}\right]^T\\
& 则另Z\geq0,x_1^2 + x_2^2 - 1\geq0\\
& 故x_1^2 + x_2^2 = 1为边界决策函数
\end{align*}
$$

### Ⅵ-Ⅳ逻辑回归损失函数分析

$$
\begin{align*}
& Training\space set: {(x^{(1)},y^{(1)}),(x^{(2)},y^{(2)}),\cdot\cdot\cdot,(x^{(m)},y^{(m)})}\\
& m\space examples\space x\in\left[\matrix{x_0 & x_1 & x_2 &\cdot\cdot\cdot&x_n}\right]^T\space\space x_0=1,y\in\{0,1\}\\
& h_\theta(x) = \frac{1}{1+e^{-\theta^Tx}}
\end{align*}
$$

思考：如何选择θ的值？
$$
\begin{align*}
& 线性回归代价函数：J(\theta) = \frac{1}{M}\sum_{i=1}^{M}\frac{1}{2}(h_\theta(x^i)-y^i)^2\\
& 定义损失项：Cost(h_\theta(x^i,y^i)) = \frac{1}{2}(h_\theta(x^i,y^i))^2\\
& 简化\Rightarrow Cost(h_\theta(x,y)) = \frac{1}{2}(h_\theta(x,y))^2
\end{align*}
$$

> 直接带入使用阶梯下降法会出现以下情况

1、因为它是间断的hx函数，所以会有多个局部最小值，故不能使用阶梯下降法

![logic_costfunction](https://gitee.com/TsingAchieve/notebook-image/raw/master/logic_costfunction.jpg)

> 逻辑回归损失函数

$$
\begin{align*}
& Cost(h_\theta(x),y) = \begin{cases}-log(h_\theta(x)) & y = 1\\-log(1-h_\theta(x)) & y = 0\end{cases}
\end{align*}
$$



#### y = 1

![logic_costfunction_2](https://gitee.com/TsingAchieve/notebook-image/raw/master/logic_costfunction_2.jpg)

#### y = 0

### Ⅵ-Ⅴ损失函数与梯度下降

$$
\begin{align*}
& 线性回归代价函数：J(\theta) = \frac{1}{M}\sum_{i=1}^{M}\frac{1}{2}(h_\theta(x^i)-y^i)^2\\
& 定义损失项：Cost(h_\theta(x^i,y^i)) = \frac{1}{2}(h_\theta(x^i,y^i))^2\\
& 简化\Rightarrow Cost(h_\theta(x,y)) = \frac{1}{2}(h_\theta(x,y))^2\\
& Cost(h_\theta(x),y) = \begin{cases}-log(h_\theta(x)) & y = 1\\-log(1-h_\theta(x)) & y = 0\end{cases}
\end{align*}
$$

- y = 0 or y = 1是恒定的，只有这两种取值

#### 损失项分段函数合并

$$
\begin{align*}
& 原式：Cost(h_\theta(x),y) = \begin{cases}-log(h_\theta(x)) & y = 1\\-log(1-h_\theta(x)) & y = 0\end{cases}\\
& 合并\Rightarrow Cost(h_\theta(x),y) = -ylog(h_\theta(x)) - (1-y)log(1-h_\theta(x))
\end{align*}
$$

- 用的思想是当某一段起作用时，将另一段想方法弄成零

#### 逻辑回归损失函数

$$
\begin{align*}
& J(\theta) = \frac{1}{M}\sum_{i=1}^{M}Cost(h_\theta(x^i),y^i)\\
& Cost(h_\theta(x),y) = -ylog(h_\theta(x)) - (1-y)log(1-h_\theta(x))\\
& \Rightarrow J(\theta) = -\frac{1}{M}\sum_{i=1}^{M}[ylog(h_\theta(x)) + (1-y)log(1-h_\theta(x))]
\end{align*}
$$

#### 梯度下降

$$
\begin{align*}
& J(\theta) = -\frac{1}{M}\sum_{i=1}^{M}[ylog(h_\theta(x)) + (1-y)log(1-h_\theta(x))]\\
& 为获取最小的误差，则使用梯度下降\\
& \theta_j = \theta_j - \alpha\frac{1}{M}\sum_{i=1}^{M}(h_\theta(x^i)-y^i)x_j^i\space 多次迭代\\
\end{align*}
$$

### Ⅵ-Ⅵ多元分类-一对多

> example

- 邮件分类：朋友、家庭、工作、爱好
- 疾病诊断：没病、冷、热
- 天气：晴天、大风、雨天、雪天

#### 原理

![multi-class](https://gitee.com/TsingAchieve/notebook-image/raw/master/multi-class.jpg)

- 假如有多个分类，那么将某一个归类为正的，然后其他为负的，算出这个正类的决策函数；依次做每个分类。

$$
h_\theta^i(x) = P(y = i|x;\theta)\space(i = 1,2,3)
$$

- 然后选择出hθ置信度最高的决策函数

## Ⅶ-过拟合问题

### Ⅶ-Ⅰ概念

- 对于见过的也就是现有的数据集拟合的很好，但是对于没见过的，拟合程度可能就会差
  - 第一个：欠拟合，拟合程度不够
  - 第二个：可能刚适合
  - 第三个：拟合过头了，对于现有的拟合的很好，但是不符合实际情况，对于别的数据集可能就没有第二个拟合的好，这就是过拟合

![过拟合例子](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E8%BF%87%E6%8B%9F%E5%90%88%E4%BE%8B%E5%AD%90.jpg)

- 代价函数很可能就为零，因为拟合的很好，所以拟合函数和实际值的差会很小

> example-one

![过拟合例子-2](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E8%BF%87%E6%8B%9F%E5%90%88%E4%BE%8B%E5%AD%90-2.jpg)

- 对于分类问题，虽然第三个拟合的很好，但是并不是我们想要的结果

### Ⅶ-Ⅱ欠拟合-过拟合解决方式

#### Ⅶ-Ⅱ-Ⅰ过拟合

- 减少特征数量
  - 人为决定哪些变量应该保留
  - 模型选择算法-自动解决去留问题
- 正则化
  - 保留所有特征变量，但是减少参数θ的大小
  - 

### Ⅶ-Ⅲ代价函数

![损失函数惩罚项](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E6%8D%9F%E5%A4%B1%E5%87%BD%E6%95%B0%E6%83%A9%E7%BD%9A%E9%A1%B9.jpg)

- 对于过拟合问题，我们在代价函数中加入了惩罚项；也就是说，如果过拟合，那么高阶部分的参数一般是不会太小的，那么加入惩罚项后，**在计算θ的时候既能使θ小，又能使损失函数的值小，这就解决的欠拟合和过拟合。**
- θ0是不用考虑加入惩罚项的。

$$
带惩罚项的损失函数：J(\theta) = \frac{1}{2M}[\sum_{i=1}^{M}(h_\theta(x^i)-y^i)^2 + \lambda\sum_{j=1}^n\theta_j^2]
$$

- 如果λ设置的值过于大，那么就对高阶项的惩罚力度过大；因为首先我们需要保证损失函数的值不能大，那么我们在设置λ的值过大时，就要使θ的值小，这样才能维持平衡，θ的值小了，那么有可能就会出现欠拟合的问题

### Ⅶ-Ⅳ线性回归的正则化

$$
\begin{align*}
& 现有损失函数：J(\theta) = \frac{1}{2M}[\sum_{i=1}^{M}(h_\theta(x^i)-y^i)^2 + \lambda\sum_{j=1}^n\theta_j^2]\\
& 使：\min_\theta J(\theta)\\
& 1、梯度下降法\\
& \theta_0 = \theta_0 - \alpha\frac{1}{M}[\sum_{i=1}^{M}(h_\theta(x^i)-y^i)x_0^i]\\
& \theta_j = \theta_j - \alpha\frac{1}{M}[\sum_{i=1}^{M}(h_\theta(x^i)-y^i)x_j^i + 2\lambda\theta_j]\Rightarrow \theta_j = (1-2\frac{\lambda}{M})\theta_j - \alpha\frac{1}{M}\sum_{i=1}^{M}(h_\theta(x^i)-y^i)x_j^i\\
& *1-2\frac{\lambda}{M}这一项一般比一略小的一个数\\
& *那么\theta_j乘以这个数就是把\theta_j在原方向稍微缩小了一点\\
& *后面的那个多项式还是和之前的梯度下降一样；总结下来就是每次更新都把\theta缩小一点点，这也加快了迭代速率\\
& 2、正规方程法\\
& \theta = [X^TX+\lambda diag(0,1,1,...,1);(n+1)维]^{-1}X……Ty
\end{align*}
$$

### Ⅶ-Ⅴ逻辑回归正则化

![逻辑回归正则化](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E9%80%BB%E8%BE%91%E5%9B%9E%E5%BD%92%E6%AD%A3%E5%88%99%E5%8C%96.jpg)
$$
\begin{align*}
& 原逻辑回归损失函数：J(\theta) = -\frac{1}{M}\sum_{i=1}^{M}[ylog(h_\theta(x)) + (1-y)log(1-h_\theta(x))]\\
& 现加入惩罚函数后的损失函数：J(\theta) = -\frac{1}{M}\sum_{i=1}^{M}[ylog(h_\theta(x)) + (1-y)log(1-h_\theta(x))]+\frac{\lambda}{2M}\sum_{j=1}^n\theta_j^2\\
& 1、梯度下降\\
& 2、正规方程法
\end{align*}
$$

## Ⅷ-神经网络

- 神经网络其实就是一组神经元连接在一起的集合

### Ⅷ-Ⅰ非线性假设

- 对于维度较少的样本，我们使用暴力穷举法倒是不错的办法，但是一般我们的样本维度是比较多的；并且对于线性回归和逻辑回归的Z的多项式，只要项数够多，那么一般都能较好的拟合数据集，但是计算过程特别慢。所以这就提出了非线性假设。

### Ⅷ-Ⅱ神经元与大脑

> 单神经元

![神经元模型展示1](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E5%85%83%E6%A8%A1%E5%9E%8B%E5%B1%95%E7%A4%BA1.jpg)

- x表示输入的维度，θ为权重也就是参数
- x输入后，经过神经元处理，处理完成输出h(x)

> 多神经元

![神经网络模型展示2](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E6%A8%A1%E5%9E%8B%E5%B1%95%E7%A4%BA2.jpg)

- x0是默认为1的
- layer1第一层称为输入层
- layer2第二层称为隐藏层（非输入输出层）
- layer3第三层也就是计算的结果，叫做输出层

> 计算步骤

$$
\begin{align*}
& a_i^j为第j层第i个神经元或单元的激活项\\
& \theta^j为第j层的权重，控制层之间的映射\\
\end{align*}
$$

![神经网络模型展示3](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E6%A8%A1%E5%9E%8B%E5%B1%95%E7%A4%BA3.jpg)
$$
\begin{align*}
& \theta_{bc}^i表示在逻辑激活函数中Z对于不同维度的参数的定位：即第i层的第b个神经元的第c个维度的权重\\
& \theta^i为权重矩阵，也就是参数矩阵
\end{align*}
$$

$$
\begin{align*}
& X = \left[\matrix{x_0 & x_1 & x_2 & x_3}\right]^T\\
& Z^{(2)} = \left[\matrix{z_1^{(2)} & z_2^{(2)} & z_3^{(2)}}\right]^T = \left[\matrix{
\theta_{10}^{(1)}x_0 + \theta_{11}^{(1)}x_1 + \theta_{12}^{(1)}x_2 + \theta_{13}^{(1)}x_3\\
\theta_{20}^{(1)}x_0 + \theta_{21}^{(1)}x_1 + \theta_{22}^{(1)}x_2 + \theta_{23}^{(1)}x_3\\
\theta_{30}^{(1)}x_0 + \theta_{31}^{(1)}x_1 + \theta_{32}^{(1)}x_2 + \theta_{33}^{(1)}x_3\\
}\right] = \left[\matrix{\theta_{1}^{(1)}x \\ \theta_{2}^{(1)}x \\ \theta_{3}^{(1)}x}\right] = \theta^{(1)}x\\
& a^{(2)} = g(z^{(2)})\\
& Z^{(3)} = \theta^{(2)}x\\
& h_\theta(x) = a^{(3)} = g(z^{(3)})
\end{align*}
$$

### Ⅷ-Ⅲ神经网络计算复杂非线性函数

> 单神经元AND示例

![and神经网络计算](https://gitee.com/TsingAchieve/notebook-image/raw/master/and%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E8%AE%A1%E7%AE%97.jpg)

- 因为属于逻辑回归的问题，所以那么我们就按照西格玛函数样子进行假设
- 得出真值表看是否符合我们的要求

> 单神经元OR示例

$$
\begin{align*}
& Simple example:OR\\
& y = x_1\space or\space x_2\\
\end{align*}
$$

![逻辑回归or示例](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E9%80%BB%E8%BE%91%E5%9B%9E%E5%BD%92or%E7%A4%BA%E4%BE%8B.jpg)

> 单神经元xnor（同或）示例

$$
\begin{align*}
& Simple example:XNOR\\
& y = x_1\space xnor\space x_2\\
\end{align*}
$$

![同或](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E5%90%8C%E6%88%96.jpg)

- 中间有隐藏层，用于计算复杂的函数

### Ⅷ-Ⅳ多元分类

我们输入多个图像：

1、人

2、小汽车

3、摩托车

4、卡车

![神经网络关系](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E5%85%B3%E7%B3%BB.jpg)

最后在输出端会输出对应结果：

output1：people?

output2：car?

output3：motorcycle?

output4：truck?

想得到如下结果：
$$
when\space people\space h_\theta(x) = \left[\matrix{1&0&0&0}\right]^T\\
when\space car\space h_\theta(x) = \left[\matrix{0&1&0&0}\right]^T\\
when\space motorcycle\space h_\theta(x) = \left[\matrix{0&0&1&0}\right]^T\\
when\space truck\space h_\theta(x) = \left[\matrix{0&0&0&1}\right]^T\\
$$

$$
\begin{align*}
& Training\space set: {(x^{(1)},y^{(1)}),(x^{(2)},y^{(2)}),\cdot\cdot\cdot,(x^{(m)},y^{(m)})}\\
& y^{(i)} one\space of\space \\
& \left[\matrix{1&0&0&0}\right]^T\\
& \left[\matrix{0&1&0&0}\right]^T\\
& \left[\matrix{0&0&1&0}\right]^T\\
& \left[\matrix{0&0&0&1}\right]^T\\
& 上述输出分类代表不同的类别
\end{align*}
$$

## Ⅸ-代价梯度无人

### Ⅸ-Ⅰ代价函数

> 解决神经网络分类问题

![神经网络关系](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E5%85%B3%E7%B3%BB.jpg)
$$
\begin{align*}
& Training\space set: {(x^{(1)},y^{(1)}),(x^{(2)},y^{(2)}),\cdot\cdot\cdot,(x^{(m)},y^{(m)})}\\
& L = Neural Network Layer-神经网络层数\\
& s_l = 第l层中不包含偏置单元的神经元数量
\end{align*}
$$

- 常规分类代价函数因为只有一个输出

$$
\begin{align*}
& 代价函数:J(\theta) = -\frac{1}{M}\sum_{i=1}^{M}[ylog(h_\theta(x)) + (1-y)log(1-h_\theta(x))]+\frac{\lambda}{2M}\sum_{j=1}^n\theta_j^2\\
\end{align*}
$$

- 多输出的神经元则需要进行改动

$$
\begin{align*}
& h_\theta(x) \in R^K \space (h_\theta(x))_i = i^{(th)}output\\
& 多输出损失函数:J(\theta) = -\frac{1}{M}\sum_{k=1}^K\sum_{i=1}^{M}[y_k^{(i)}log(h_\theta(x^{(i)}))_k + (1-y_k^{(i)})log(1-h_\theta(x^{(i)}))_k]+\frac{\lambda}{2M}\sum_{l=1}^{L-1}\sum_{i=1}^{s_l}\sum_{j=1}^{s_{l+1}}(\theta_{ji}^{(l)})^2\\
& 1、因为是多个输出，那么损失函数在求值的时候就应该把所有的输出加在一起求出最小的J，这样的J就满足最小的损失（相对的是多输出函数，不用除以K）\\
& 2、K代表输出的个数\\
& 3、\theta角标（l代表第l层的权重，j代表第j个神经元，i代表权重列的第i个输入）\\
& 4、权重参数只有L-1层，也就是总层数个数减一\\
& 5、s_l为l层中不含偏置单元的神经元数量，又因为每一层神经元参数又不止一个，所以就有了i，\\
& i表示每一层所有的\theta参数都包含完\\
\end{align*}
$$

![multi-neurons-cost-puish](E:\FileData\StudyResource\PythonStudyResource\machine-learning\static\images\multi-neurons-cost-puish.png)

### Ⅸ-Ⅱ反向传播算法

$$
\begin{align*}
& h_\theta(x) \in R^K \space (h_\theta(x))_i = i^{(th)}output\\
& 多输出损失函数:J(\theta) = -\frac{1}{M}\sum_{k=1}^K\sum_{i=1}^{M}[y_k^{(i)}log(h_\theta(x^{(i)}))_k + (1-y_k^{(i)})log(1-h_\theta(x^{(i)}))_k]+\frac{\lambda}{2M}\sum_{l=1}^{L-1}\sum_{i=1}^{s_l}\sum_{j=1}^{s_l+1}(\theta_{ji}^{(l)})^2\\
& 求minJ(\theta)\\
& 梯度下降法:\theta_{ji}^l = \theta_{ji}^l - \alpha\frac{\partial J(\theta)}{\partial\theta_{ji}^l}
\end{align*}
$$

![反向传播梯度下降神经元表示](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E5%8F%8D%E5%90%91%E4%BC%A0%E6%92%AD%E6%A2%AF%E5%BA%A6%E4%B8%8B%E9%99%8D%E7%A5%9E%E7%BB%8F%E5%85%83%E8%A1%A8%E7%A4%BA.png)

#### Ⅸ-Ⅱ-Ⅰ反向传播概念

对每个节点，我们计算这样一项

![神经网络关系](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E7%A5%9E%E7%BB%8F%E7%BD%91%E7%BB%9C%E5%85%B3%E7%B3%BB.jpg)
$$
\begin{align*}
& \delta_j^{(l)}代表了第l层的第j个节点的误差\\
& \alpha_j^{(l)}代表了第l层第j个神经元的激活值\\\\
& 以上述右侧四个神经元网络结构为例子\\
& \delta_j^{(4)} = \alpha_j^{(4)} - y_j（也就是用于判断我们这隐藏层与实际的误差有多少）\\
& \delta^{(3)} = (\theta^{(3)})^T\delta^{(4)}*g^{'}(z^{(3)})\\
& \delta^{(2)} = (\theta^{(2)})^T\delta^{(3)}*g^{'}(z^{(2)})
\end{align*}
$$

#### Ⅸ-Ⅱ-Ⅱ反向传播算法实现

$$
\begin{align*}
& Training\space set: {(x^{(1)},y^{(1)}),(x^{(2)},y^{(2)}),\cdot\cdot\cdot,(x^{(m)},y^{(m)})}\\
& set \space \delta_{ij}^{(l)} = 0\\
& 遍历训练样本，也就是让每个样本都走一次神经网络\\
& 设置第一层神经单元，也就是输入层(a_i^1 = x^i)\\
& 使用y^i,计算\delta^L = \alpha^L - y^i\\
& 计算正向值的激活单元\alpha_i^l\\
& 计算\delta^{L-1},\delta^{L-2},\delta^{L-3},\delta^{L-4},\cdots,\delta^2\\
& \delta_{ij}^l = \delta_{ij}^l + \alpha_j^l\delta_i^{l+1}[(\alpha_j^l\delta_i^{l+1}这是J的偏导数，这里的i是第几个神经元，j表示第几个\theta传递参数)\\
& 这个地方也就是记录不同样本的相同位置的误差和]\\
& D_{ij}^{(l)} = \frac{1}{m}\delta_{ij}^{(l)} + \lambda\theta_{ij}^{(l)}\space (if j\space \neq 0 )\\
& D_{ij}^{(l)} = \frac{1}{m}\delta_{ij}^{(l)}\space (if j\space = 0 )\\
\end{align*}
$$

### Ⅸ-Ⅲ理解反向传播

**参考代价函数底部大图**，从输入传到最终输出。
$$
\begin{align*}
&* 先只看单一的神经元输出\\
& 多输出损失函数:J(\theta) = -\sum_{i=1}^{M}[y_{(i)}log(h_\theta(x^{(i)})) + (1-y^{(i)})log(1-h_\theta(x^{(i)}))]+\frac{\lambda}{2M}\sum_{l=1}^{L-1}\sum_{i=1}^{s_l}\sum_{j=1}^{s_l+1}(\theta_{ji}^{(l)})^2\\
& 我们先假设没有惩罚项:J(\theta) = -\sum_{i=1}^{M}[y^{(i)}log(h_\theta(x^{(i)})) + (1-y^{(i)})log(1-h_\theta(x^{(i)}))]\\
& 将:y^{(i)}log(h_\theta(x^{(i)})) + (1-y^{(i)})log(1-h_\theta(x^{(i)})) = cost(i)\\
& cost(i)\approx (h_\theta(x^i) - y^i)^2\\
& \delta_i^{(l)}为神经元\alpha_j^{(l)}的误差\\
& \delta_j^{(l)} = \frac{\partial cost(i)}{\partial_{z_j^{(l)}}}\space (j \geq 0)\space\\
& cost(i)为第i个样本的损失项，可以通过前项传播得出
\end{align*}
$$
![反向传播理解1](E:\FileData\StudyResource\PythonStudyResource\machine-learning\static\images\反向传播理解1.png)

![反向传播理解2](https://gitee.com/TsingAchieve/notebook-image/raw/master/%E5%8F%8D%E5%90%91%E4%BC%A0%E6%92%AD%E7%90%86%E8%A7%A32.png)

